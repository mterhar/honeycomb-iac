# Honeycomb IaC

Using Honeycomb's Terraform Modules for Infrastructure as Code (IaC) is a great way to standardize an organization's use of Honeycomb.
Creating Honeycomb resources with Terraform is pretty easy and is well documented in [the provider reference docs](https://registry.terraform.io/providers/honeycombio/honeycombio/latest/docs).

The problem with using Terraform for Honeycomb resources is that it's never how teams get started.
Teams start with creating a team and sending events.
They use the Query UI to look at their data and find patterns.
They create derived columns to improve their ability to find interesting information.
They create boards around recurring themes.
They create triggers to catch known issues before things get crazy.
They create SLOs to catch unknown issues before things get crazy.

Throughout this process, where real value is unlocked, the explorations are necessarily driven by the Honeycomb UI.

Once a team has reached a maturity level where the scale of managing these resources is painful and tedious, they reach for Terraform.

## Plan A, make it again

Most teams will then create resources in terraform, apply them so terraform can manage them, and then delete and clean up the old stuff manually.
This isn't terrible but it takes time and crafting the resources can lead to unexpected outcomes where definitions differ slightly.

This is made worse when the team creating the terraform objects isn't the same one that created the Honeycomb objects.
It's tougher to create things the right way when syntactically it's different and the underlying reasons aren't known.

## Plan B, extract the resources with Terraform

Now that [Honeycomb is in Terraformer][https://github.com/GoogleCloudPlatform/terraformer/tree/master/providers/honeycombio],
we can use it to extract a lot of the resources for us. 
This reduces the guesswork on how to define a honeycomb resource to match one created with the UI.

It's not perfect, though. Like all IaC it requires a bit of clean-up to make it resuable in other places.

If the goal is to extract the configurations and manage them in Git without moving them or creating abstract reusables, just follow the first extraction step and you're good.

If the goal is to have a clean, abstractable, reusable set of resources for creating and managing Honeycomb objects, the later sections will help you get there.

## Extraction

The extraction process has nice, clear steps.

### Install terraform

```shell
export PROVIDER=honeycombio
curl -LO https://github.com/GoogleCloudPlatform/terraformer/releases/download/$(curl -s https://api.github.com/repos/GoogleCloudPlatform/terraformer/releases/latest | grep tag_name | cut -d '"' -f 4)/terraformer-${PROVIDER}-linux-amd64
chmod +x terraformer-${PROVIDER}-linux-amd64
sudo mv terraformer-${PROVIDER}-linux-amd64 /usr/local/bin/terraformer
```

### Extract the Honeycomb resources

Go into your current [Honeycomb team](https://ui.honeycomb.io/teams) and create an API Key for the environment you want to extract.
Give it _ALL_ the permissions.

Put the honeycomb API key into an environment variable and run terraformer like this:

```shell
HONEYCOMB_API_KEY=$HONEYCOMB_API_KEY \
  terraformer import honeycombio \
  --resources="trigger,derived_column,slo,query,recipient" \
  --path-pattern "{output}" \
  --path-output output
```

replace the `output/providers.tf` with:

```json
provider "honeycombio" {
  api_url = "https://api.honeycomb.io"
}

terraform {
  required_providers {
    honeycombio = {
      source  = "honeycombio/honeycombio"
      version = "~> 0.11.2"
    }
  }
}
```

**Replace the 0.11.2 version with [the latest](https://registry.terraform.io/providers/honeycombio/honeycombio/).**

The extracted provider is called `honeycomb` rather than `honeycombio/honeycombio` so we have to run this:

```shell
$ cd output
$ terraform state replace-provider -- -/honeycombio honeycombio/honeycombio
Terraform will perform the following actions:

  ~ Updating provider:
    - registry.terraform.io/-/honeycombio
    + registry.terraform.io/honeycombio/honeycombio

Changing 50 resources:

...

Do you want to make these changes?
Only 'yes' will be accepted to continue.

Enter a value: yes

Successfully replaced provider for 50 resources.
```

```shell
$ terraform providers                                                      

Providers required by configuration:
.
├── provider[terraform.io/builtin/terraform]
└── provider[registry.terraform.io/honeycombio/honeycombio] ~> 0.11.2

Providers required by state:

    provider[registry.terraform.io/honeycombio/honeycombio]

```

### Validate the extracted resources

Inside the `output` directory, run `terraform init`. If the changes above worked, it'll successfully initialize.

```
$ terraform init
Initializing the backend...

Initializing provider plugins...
- terraform.io/builtin/terraform is built in to Terraform
- Finding honeycombio/honeycombio versions matching "~> 0.11.2"...
- Installing honeycombio/honeycombio v0.11.2...
- Installed honeycombio/honeycombio v0.11.2 (signed by a HashiCorp partner, key ID 79C9C3A7AFA7A9DD)

Partner and community providers are signed by their developers.
If you'd like to know more about provider signing, you can read about it here:
https://www.terraform.io/docs/cli/plugins/signing.html

Terraform has created a lock file .terraform.lock.hcl to record the provider
selections it made above. Include this file in your version control repository
so that Terraform can guarantee to make the same selections by default when
you run "terraform init" in the future.

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.
```

After initialization succeeds, you can check the resources themselves are valid and match what is present in Honeycomb.

```shell
$ terraform validate
Success! The configuration is valid.

$ terraform plan
data.terraform_remote_state.local: Reading...
data.terraform_remote_state.local: Read complete after 0s
...
No changes. Your infrastructure matches the configuration.

Terraform has compared your real infrastructure against your configuration and found no differences, so no changes are needed.
```

If there are changes, it's likely that a character in a column name, slo, trigger, etc was extracted oddly and doesn't match the current configuration.

### Commit to a new git repo

If everything is working well this far, it's time for regular IaC practices so throw that stuff in a repostory.

You probably want to output folder name to be something more reasonable like `honeycomb-iac`.

```console
cd .. && mv output honeycomb-iac && cd honeycomb-iac
git init
echo ".terraform*\nterraform.tfstate*" > .gitignore
git add .
git commit -m "Initial terraform resource extraction"
```

Add a remote with `git remote add origin https://github.com/...` and `git push`.

Then it's automation, pipelines, tftest, IaC security tests, and the sky is the limit! 

If you're unsatisfied and want to do cool stuff like "move the configuration" or "make it reusable", let's continue!

## Refactoring and clean-up

### Removing stuff

### Considerations for targeting a different team

## Plan and apply the new resources

Using `terraform plan`, you can iterate through the rough edges of the configuration to ensure you're not creating duplicates or misnaming resources.